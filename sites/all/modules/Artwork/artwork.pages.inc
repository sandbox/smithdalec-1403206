<?php

/**
 * Generates a bulleted list of menu items that are immediate children of the
 * current page being viewed.
 * @return type 
 */
function artwork_add_page() {
  // Returns the current menu item
  $item = menu_get_item();
  // Gets the children of an arbitrary menu item
  $links = system_admin_menu_block($item);
  
  foreach ($links as $link) {
    $items[] = l($link['title'], $link['href'],
                 // Allows the menu system to handle what attributes the link
                 // should have, any interaction witht he translation system, etc.
                 $item['localized_options'])
                 // Strip out any unsafe HTML tags
                 . ': ' . filter_xss_admin($link['description']);
  }
  
  return theme('item_list', array('items' => $items));
}

/**
 * Present an artwork submission form.
 *
 * @global type $user
 * @param type $type
 * @return string 
 */
function artwork_add($type) {
  global $user;
  
  $types = artwork_types();
  $type = isset($type) ? str_replace('-', '_', $type) : NULL;
  // If no such artwork type, return a 404 page
  if (empty($types[$type])) {
    return MENU_NOT_FOUND;
  }
  
  // Create a new, empty artwork object
  $artwork = entity_get_controller('artwork')->create($type);
  
  // Set page title
  // PASS_TRHOUGH as the second param tells the function to allow HTML in the
  // string we're giving because we've already checked to make sure it's safe.
  drupal_set_title(t('Create @name', array('@name' => $types[$type]->name)), PASS_THROUGH);
  
  // Display the form
  return drupal_get_form($type . '_artwork_form', $artwork);
}

/**
 * Presents the artwork editing form, or redirects to delete confirmation.
 *
 * @param type $artwork
 * @return type 
 */
function artwork_page_edit($artwork) {
  $types = artwork_types();
  drupal_set_title(t('<em>Edit @type</em> @title', array('@type' =>
    $types[$artwork->type]->name, '@title' =>$artwork->title)), PASS_THROUGH);
  
  return drupal_get_form($artwork->type . '_artwork_form', $artwork);
}

/**
 * Form builder; Displays the artwork add/edit form.
 *
 * @param $form
 * @param $form_state
 * @param $artwork
 *   The artwork object to edit, which may be brand new.
 */
function artwork_form($form, &$form_state, $artwork) {
  // Set the id to identify this as an artwork edit form.
  $form['#id'] = 'artwork-form';
  
  // Save the artwork for later, in case we need it.
  $form['#artwork'] = $artwork;
  $form_state['artwork'] = $artwork;
  
  // Common fields. We don't have many.
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => $artwork->title,
    '#weight' => -5,
    '#required' => TRUE,
  );
  
  // Checkbox to indicate whether we should save a new revision or overwrite the
  // existing revision.
  $form['revision'] = array(
    '#access' => user_access('administer artworks'),
    '#type' => 'checkbox',
    '#title' => t('Create new revision'),
    '#default_value' => 0,
  );
  
  // Add the buttons.
  // Save button is always available
  $form['buttons'] = array();
  $form['buttons']['#weight'] = 100;
  $form['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 5,
    '#submit' => array('artwork_form_submit'),
  );
  // Delete button is available if we're editing an existing artwork.
  if (!empty($artwork->aid)) {
    $form['buttons']['delete'] = array(
      '#access' => user_access('delete artworks'),
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#weight' => 15,
      '#submit' => array('artwork_form_delete_submit'),
    );
  }
  
  $form['#validate'][] = 'artwork_form_validate';
  
  // Passes our form off to the field system so that any fields we've added to
  // this artwork type can be added to the form.
  field_attach_form('artwork', $artwork, $form, $form_state);
  
  return $form;
}

/**
 * Let the Form API validate our form for us.
 * 
 * @param type $form
 * @param type $form_state 
 */
function artwork_form_validate($form, &$form_state) {
  $artwork = $form_state['artwork'];
  
  // Field validation.
  field_attach_form_validate('artwork', $artwork, $form, $form_state);
}

/**
 * The Submit callback is doing extremely little. It's just glue code between
 * the form and the artwork_save() function.
 *
 * @global type $user
 * @param type $form
 * @param array $form_state 
 */
function artwork_form_submit($form, &$form_state) {
  global $user;
  
  // Pull the old artwork object out of the $form_state variable.
  $artwork = &$form_state['artwork'];
  
  // Set the artwork's uid if it's being created at this time.
  if (empty($artwork->uid)) {
    $artwork->uid = $user->uid;
  }
  
  $artwork->title = $form_state['values']['title'];
  $artwork->revision = $form_state['values']['revision'];
  
  // Notify field widgets.
  field_attach_submit('artwork', $artwork, $form, $form_state);
  
  // Save the artwork.
  artwork_save($artwork);
  
  // Notify the user.
  drupal_set_message(t('Artwork saved.'));
  
  $form_state['redirect'] = 'artwork/' . $artwork->aid;
}

/**
 * Redirects the user to artwork/$aid/delete
 *
 * @param type $form
 * @param type $form_state 
 */
function artwork_form_delete_submit($form, &$form_state) {
  $destination = array();
  if (isset($_GET['destination'])) {
    $destination = drupal_get_destination();
    unset($_GET['destination']);
  }
  $artwork = $form['#artwork'];
  $form_state['redirect'] = array('artwork/' . $artwork->aid . '/delete',
      array('query' => $destination));
}

/**
 * Displays an artwork; Hands data off to the Field API
 *
 * @param type $artwork
 * @param type $view_mode View mode defined in artwork_entity_info().
 * @return type 
 */
function artwork_page_view($artwork, $view_mode = 'full') {
  // Remove previously built content, if exists.
  $artwork->content = array();
  
  // If we're in teaser mode, we want to inject the title of the artwork into
  // the viewable result.
  if ($view_mode == 'teaser') {
    $artwork->content['title'] = array(
      '#markup' => filter_xss($artwork->title),
      'weight' => -5,
    );
  }
  
  // Build fields content. Standard stuff.
  field_attach_prepare_view('artwork', array($artwork->aid => $artwork), $view_mode);
  entity_prepare_view('artwork', array($artwork->aid => $artwork));
  $artwork->content += field_attach_view('artwork', $artwork, $view_mode);
  
  return $artwork->content;
}

/**
 * Rather than build a complete form, we will simply pass data on to a utility
 * function of the Form API called confirm_form().
 *
 * @param type $form
 * @param type $form_state
 * @param type $artwork
 * @return type 
 */
function artwork_delete_confirm($form, &$form_state, $artwork) {
  $form['#artwork'] = $artwork;
  // Always provide entity id in the same form key as in the entity edit form
  $form['aid'] = array('#type' => 'value', '#value' => $artwork->aid);
  return confirm_form($form,
    t('Are you sure you want to delete %title?', array('%title' => $artwork->title)),
    'artwork/' . $artwork->aid,
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

function artwork_delete_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    $artwork = artwork_load($form_state['values']['aid']);
    artwork_delete($form_state['values']['aid']);
    watchdog('artwork', '@type: deleted %title.',
        array('@type' => $artwork->type, '%title' => $artwork->title));
    
    $types = artwork_types();
    drupal_set_message(t('@type %title has been deleted.',
        array(
          '@type' => $types[$artwork->type]-> name, 
          '%title' => $artwork->title)
        )
    );
  }
  
  $form_state['redirect'] = '<front>';
}